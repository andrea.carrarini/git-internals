package core;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.zip.InflaterInputStream;

/**
 * Created by andrea on 14/01/19.
 */
public class GitRepository {

    private String repositoryPath;
    private static  String pathToDirectory = "/home/andrea/universita/svigruppo/labs/git-internals/";
    //private String sample_repos_path = "/sample_repos/sample01";
    private static String masterCommitPath = "refs/heads/master";
    private String pathToMaster;


    public String getRefHash(String masterCommit) {

        String masterCommitPath = pathToDirectory + repositoryPath + "/" +  masterCommit;
        try {
            return new BufferedReader(new FileReader(new File(masterCommitPath))).readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "ERROR";
    }

    public GitRepository(String repositoryPath) {
        this.repositoryPath = repositoryPath;
        this.pathToMaster = pathToDirectory + repositoryPath + "/" + masterCommitPath;
    }

    public String getHeadRef() {
        return this.masterCommitPath;
    }

    public String getPathToMaster() {
        return pathToMaster;
    }
}