package core;

import java.io.*;
import java.util.zip.InflaterInputStream;

/**
 * Created by andrea on 20/01/19.
 */
public class GitObject {

    protected String repositoryPath;
    protected String hash;
    protected String type;
    protected String content;
    protected static  String pathToDirectory = "/home/andrea/universita/svigruppo/labs/git-internals/";

    public GitObject(String repositoryPath, String hash) {
        this.repositoryPath = repositoryPath;
        this.hash = hash;
        this.content = readObject();
        this.type = content.substring(0, content.indexOf(" "));
    }

    public GitObject() {
    }

    public String readObject() {

        /* Taking the first 2 chars to know in which dir the object is located */
        String objPath = pathToDirectory + repositoryPath + "/objects/" + hash.substring(0, 2) + '/' + hash.substring(2, hash.length());
        String finalString = decompress(objPath);
        return finalString;
    }

    /* same as $git cat-file -p hashName */
    public String decompress(String filePath) {

        try {

            InputStream input = new FileInputStream(new File(filePath));
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            InflaterInputStream decompresser = new InflaterInputStream(input);
            byte[] buffer = new byte[1];
            int len;

            while((len = decompresser.read(buffer)) != -1)
                output.write(buffer, 0, len);

            String finalString = output.toString("UTF-8");
            return finalString;

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "ERROR";
    }

    public String getHash() {
        return hash;
    }

    public String getType() {
        return type;
    }

    public String getContent() {
        return content;
    }
}
