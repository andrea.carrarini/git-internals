package core;

import java.io.*;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

/**
 * Created by andrea on 17/01/19.
 */
public class GitBlobObject extends GitObject{

    private String repositoryPath;
    private String objDirectory;

    public GitBlobObject(String repositoryPath, String hash) {
        super(repositoryPath, hash);
    }

    public String getType() {
        return this.type;
    }

    public String getContent() {
        return this.content.substring(this.content.indexOf("\0") + 1);
    }
}
