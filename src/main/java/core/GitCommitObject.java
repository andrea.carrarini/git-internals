package core;

import java.util.regex.Pattern;

/**
 * Created by andrea on 20/01/19.
 */
public class GitCommitObject extends GitObject{

    /* repositoryPath is sample_repos/sample01 */
    private String repositoryPath;
    private String hash;
    private String treeHash;
    private String parentHash;
    private String author;
    private String committer;

    public GitCommitObject(String repositoryPath, String masterCommitHash) {
        super(repositoryPath, masterCommitHash);
        setTreeHash();
        setParentHash();
        setAuthor();
        setCommitter();
    }

    /*public String getMasterCommitData(String requestedData) {

        GitRepository gitRepository = new GitRepository(repositoryPath);
        String pathToMaster = gitRepository.getPathToMaster();
        String readData = null;
        try {
            readData = gitRepository.decompress(gitRepository.readFile(pathToMaster, 0, 0));
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(gitRepository.getPathToMaster());
        System.out.println(readData);

        *//* splts the content in lines *//*
        String[] parts = readData.split(Pattern.quote("\n"));

        switch (requestedData) {
            case "treeHash": {
                String treeHash = parts[0];
                return treeHash;
            }
            case "parentHash": {
                String parentHash = parts[1];
                return parentHash;
            }
            case "author": {
                String author = parts[2];
                return author;
            }
            case "committer": {
                String committer = parts[3];
                return committer;
            }
        }
        return "ERROR in getMasterCommitData";
    }

    public String getTreeHash() {

        String treeHash = getMasterCommitData("treeHash");
        return treeHash.substring(5);
    }

    public String getParentHash() {

        String parentHash = getMasterCommitData("parentHash");
        return parentHash.substring(7);
    }

    public String getAuthor() {
        String author = getMasterCommitData("author");
        return author.substring(7, author.length() - 17);
    }*/

    public String getTreeHash() {
        return this.treeHash;
    }

    public String getParentHash() {
        return this.parentHash;
    }

    public String getAuthor() {
        return this.author;
    }

    public String getCommitter() {
        return this.committer;
    }

    public void setTreeHash() {
        /* splts the content in lines */
        String[] parts = this.getContent().split(Pattern.quote("\n"));
        this.treeHash = parts[0].substring(parts[0].lastIndexOf(" ") + 1);
    }

    public void setParentHash() {
        String[] parts = this.getContent().split(Pattern.quote("\n"));
        this.parentHash = parts[1].substring(parts[1].indexOf(" ") + 1);
    }

    public void setAuthor() {
        String[] parts = this.getContent().split(Pattern.quote("\n"));
        this.author = parts[2].substring(parts[2].indexOf(" ") + 1, parts[2].lastIndexOf(">") + 1);
    }

    public void setCommitter() {
        this.committer = committer;
    }
}
